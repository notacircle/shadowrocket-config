# shadowrocket-config

## Overview

This repository is made to store and share Shadowsocks configuration files.

## Configs

### Selective (only specific sites banned from Russia)

[selective-config.conf](https://gitlab.com/notacircle/shadowrocket-config/-/raw/main/selective/selective-config.conf)
